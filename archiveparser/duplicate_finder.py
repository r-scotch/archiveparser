import hashlib
from tabulate import tabulate
from archiveparser import ArchiveParser

def hash_review(review):
    h = hashlib.md5()
    h.update(review['reviewer'].lower().encode('utf-8'))
    h.update(review['link'].lower().encode('utf-8'))
    h.update(review['bottle'].lower().encode('utf-8'))
    return h.hexdigest()

def find(input_file='archive.csv', output_file=None):
    parser = ArchiveParser(input_file)
    archive = parser.get_rows()
    hash_set = set()
    first = next(archive)
    hash_set.add(hash_review(first))
    errors = []
    for index, review in enumerate(archive):
        index += 1
        if review['date'] is None:
            errors.append([
                "No date",
                index, 
                review['reviewer'].encode('utf-8'),
                review['bottle'].encode('utf-8')
            ])

        review_hash = hash_review(review)
        if review_hash in hash_set:
           errors.append([
                "Duplicate",
                index, 
                review['reviewer'].encode('utf-8'),
                review['bottle'].encode('utf-8')
            ])
        else:
            hash_set.add(review_hash)
    if output_file:
        with open(output_file, 'a') as f:
            f.write(tabulate(errors, headers=['Error Type', 'Row', 'Reviewer', 'Bottle']))
    return errors
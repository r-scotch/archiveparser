import csv, praw, requests, re, logging
from http.cookiejar import CookieJar
from urllib.request import build_opener, HTTPCookieProcessor
from datetime import date, datetime

class ArchiveParser():
    def __init__(self, archive_filename):
        self.filename = archive_filename
        self.date_pattern = re.compile(r'(\d{1,2})[^\d](\d{1,2})[^\d](\d{2,4})')
        self.url_pattern = re.compile(r'^https?://www\.')
        self.i18n_pattern = re.compile(r'(https?://www\.)[a-z]{2}\.(reddit.*)')
        self.shortlink_pattern = re.compile(r'(redd\.it)')
        self.subreddit_pattern = re.compile(r'reddit\.com/r/([a-z]+)/')

    def fix_url(self, url):
        """ Appends `https://wwww.` and removes any internationalisation
        from the domain (e.g. <pt.>reddit.com). """
        short_match = self.shortlink_pattern.search(url)
        if short_match:
            url = self.shortlink_pattern.sub('reddit.com', url)
        if not self.url_pattern.match(url):
            if url.startswith('http'):
                url = 'https://www.' + url[7:]
            elif url.startswith('www.'):
                url = 'https://' + url
            else:
                url = 'https://www.' + url
        url = self.i18n_pattern.sub(r'\1\2', url)
        return url

    def fix_username(self, username):
        prefix = '/u/'
        username = username.strip() # strip whitespace
        username = username.strip(prefix) # strip prefix
        return username

    def parse_date(self, date_string):
        """Gets the year, month and day from a string in mm/dd/yy(yy) format
        Asserts the string is in mm/dd/yy or mm/dd/yyyy format
        and returns a tuple containing the year (yyyy), month (mm) and day (dd).
        If the string happens to be in dd/mm/yy format, it switches the day and month.
        Args:
            date_string: (string) Date in mm/dd/yy or mm/dd/yyyy format
        Returns:
            datetime.date object
        """
        match = self.date_pattern.match(date_string)
        if match:
            month, day, year = map(int, match.groups())
            # swap month and day if month is greater than 12
            if month > 12:
                month, day = day, month
            # add millenials if year is represented in two or three digits
            if year < 1000:
                year += 2000
            try:
                review_date = date(year, month, day)
                # if review_date is in the future
                if review_date > datetime.now().date(): 
                    logging.error("Date lies in the future: " + date_string)
                    return None
                return review_date
            except ValueError:
                logging.error("Invalid date format: " + date_string)
            except Exception as e:
                logging.exception("Unexpected exception:", e)
        return None

    def get_rows(self):
        fields = ['timestamp', 'bottle', 'reviewer', 'link', 'rating', 'region', 'price', 'date']
        with open(self.filename, encoding='utf-8', mode='r') as archive:
            reader = csv.DictReader(archive, fieldnames=fields, delimiter=',')
            headers = next(reader)
            for row in reader:
                # convert everything to lowercase
                row = { k: v.lower() for k, v in row.items() }

                # parse to appropriate objects
                row['timestamp'] = self.parse_date(row['timestamp'])
                row['date'] = self.parse_date(row['date'])
                row['link'] = self.fix_url(row['link'])
                row['reviewer'] = self.fix_username(row['reviewer'])
                row['subreddit'] = self.get_subreddit(row)
                yield(row)

    def get_subreddit(self, row, subreddits=[]):
        row['link'] = row['link'].lower()
        match = self.subreddit_pattern.search(row['link'])
        if match and (not subreddits or match.group(1) in subreddits):
            return match.group(1)
        return None

    def get_submission(self, row, reddit):
        try:
            submission = reddit.get_submission(row['link'])
            return submission
        except (requests.exceptions.RequestException, praw.errors.RedirectException):
            logging.error('Unable to retrieve submission: ' + row['link'])
        except ValueError:
            logging.error('Invalid url: ' + row['link'])
            

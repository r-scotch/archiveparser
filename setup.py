from distutils.core import setup
version = '1.0.2'
setup(
    name = 'archiveparser',
    packages = ['archiveparser'], # this must be the same as the name above
    version = version,
    description = 'Archive Parser for /r/Scotch',
    author = 'Stan Janssen',
    author_email = 'stan@janssen.io',
    url = 'https://gitlab.com/r-scotch/archiveparser', # use the URL to the github repo
    download_url = 'https://gitlab.com/r-scotch/archiveparser/repository/archive.tar.gz?ref=' + version, # I'll explain this in a second
    keywords = ['archive parser', 'scotch', 'reddit'], # arbitrary keywords
    classifiers = [],
    install_requires=[
          'tabulate', 'requests', 'praw'
      ],
)

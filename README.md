# /r/scotch - ArchiveParser

Parses the Whisky Review Archive ([/r/scotch](https://reddit.com/r/scotch), [/r/bourbon](https://reddit.com/r/bourbon), [/r/worldwhisky](https://reddit.com/r/worldwhisky)]). Assumes the following order of columns:   
Timestamp, Bottle, Reviewer Username, Link To reddit Review, Rating, Region, Price, Date

Tries to fix incorrect dates and URLs. Also adds the subreddit (retrieved from the link) to which it was submitted.

## Installation
1. Install using pip: `pip install https://gitlab.com/r-scotch/archiveparser/repository/archive.tar.gz`
2. Download archive as csv file.

## Usage
```python
    from archiveparser import ArchiveParser
    from praw import Reddit
    parser = ArchiveParser(archive_filename='archive.csv')
    rows = parser.get_rows() # returns generator
    
    reddit = Reddit('My Very Descriptive User Agent')
    row = next(rows)
    submission = parser.get_submission(row, reddit)
```

## Todo
Add some more proper documentation in the code.
